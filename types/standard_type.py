import random

x = None

if x:
    print("Do you think None is True?")
elif x is False:
    print("Do you think None is False?")
else:
    print("None is not True, or False, None is just None...")

# Numbers

1  # int
True  # boolean
1e5  # float
complex(1, 3)  # complex
x = 3+5j

# Immutable sequences

# Strings
"This is a string"
result = 1
f"This is the result: {result}"

# Tupples
a = 2,
b = ('A', 3, 5, 6)
b[0]
b[:3]

# Bytes
bytes([15, 15])
bytes('a', encoding='utf8')

# Mutable sequences

# Lists
my_list = [4, 2, 3]
my_list_2 = [1, 'a', 3]
my_list_3 = [(1, 2), (2, 3)]

# Bytearray
bytearray([15, 15])
bytearray('a', encoding='utf8')

# Sets

my_set = {'a', 'b', 'c'}
'a' in my_set
my_set_2 = {'b', 'c'}
my_set.union(my_set_2)
my_set_2.add('e')
my_set_2.remove('b')

fs = frozenset({'a', 'b', 'c'})
# fs.remove('b') fails

# Mappings

cities = {'Wales': 'Cardiff',
          'England': 'London',
          'Scotland': 'Edinburgh',
          'Northern Ireland': 'Belfast',
          'Ireland': 'Dublin'}

lista = []
lista.append("hola")



print(len(cities))
cities['Wales']
print(cities.get('spain'))
cities.get('Ireland')

print(cities.values())  # View object not list!
for city_value in cities.values():
    print(city_value)
print(cities.keys())  # view object too! NOT LIST!
for city_key in cities.keys():
    print(city_key)
print(cities.items())  # key, value tuples dict_view type->dict_items
for key_value in cities.keys():
    print(key_value)
print('Wales' in cities)
cities.update({"Wales": ['Hello', "world"]})
print(type(cities['Wales']))


# Callables

def print_msg():
    print('Hello World!')


print_msg()
print(type(print_msg))


def print_my_msg(nombre, apellido):
    """
    Escribe todos los mensajes que se le pasen a la funcion
    :param messages:
    :return: None
    """
    print(nombre, apellido)

def make_list_range(start, end):


    return list(range(start, end))


print_my_msg(apellido='Hello World')
# print_my_msg("Hello world", "This is another world")

lista = make_list_range(2, 4)
print(lista)

def square(n):
    return n * n


# Store result from square in a variabl
result = square(4)
print(result)

# tuple of vowels
vowels = ('a', 'e', 'i', 'o', 'u')

fSet = frozenset(vowels)
print('The frozen set is:', fSet)
print('The empty frozen set is:', frozenset())

# frozensets are immutable
# fSet.add('v')

# print(fSet)

# print({"hello": "rose"} | {"hello": "Jhon"})
# print({"hello": "rose"} | {"bye": "Jhon"})

d = {"name": "Jhon", ("surname",): 1}

d.update([(('y', 'x'), ["Other value"])])

print(d[('y', 'x')])

print(d)
