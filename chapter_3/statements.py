a = int("3")
b: str = 3
c = ["a"]

print(type(a))
print(type(b))
b = "Esto es un dato nuevo"
print(type(b))
a + 3
3 + 2

# i += 1
# j *= 1

assert a == 3


def f(x):
    return x + 1

del(a)


def infinite_sequence():
    num = 0
    while True:
        yield num
        num += 1

gen = infinite_sequence()
print("Generadores")
print(next(gen))
next(gen)
print("finish generadores")


def func():
    # Something bad here, I cannot open the database
    raise IOError

func()
try:
    pass
except IOError as exc:
    print(exc)

for val in "string":
    if val == "i":
        break
    print(val)

print("The end")


for val in "string":
    if val == "i":
        continue

    print(val)

print("The end")