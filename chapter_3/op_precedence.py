# Precedence of or & and
meal = "fruit"

money = 0
_meal_condition = meal == "fruit" or meal == "sandwich"
if _meal_condition and money >= 2: # a or b and c ( a or b ) and c => (x and b)
    print("Lunch being delivered")
else:
    print("Can't deliver lunch")


# Precedence of or & and
meal = "fruit"

money = 0

if (meal == "fruit" or meal == "sandwich") and money >= 2:
    print("Lunch being delivered")
else:
    print("Can't deliver lunch")