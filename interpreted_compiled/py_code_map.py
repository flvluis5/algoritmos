# Inspired by https://towardsdatascience.com/run-your-python-code-as-fast-as-c-4ae49935a826

# /Users/rlucerga/Software/pypy3.7-v7.3.5-osx64/bin
# Follow instructions in https://doc.pypy.org/en/latest/install.html
# sudo ln -s /Users/rlucerga/Software/pypy3.7-v7.3.5-osx64/bin/pypy /usr/local/bin/pypy
# https://pypi.org/project/pipx/
# brew install pipx
# pipx ensurepath

# Install virtualenv
# https://virtualenv.pypa.io/en/latest/installation.html
# pipx install virtualenv
# virtualenv --help

# pypy -mpip install virtualenv


# Create virtual environment in /Users/rlucerga/.virtualenvs
# cd /Users/rlucerga/.virtualenvs
# virtualenv -p pypy base-pypy

# Activate with
# source base-pypy/bin/activate


import time

start = time.time()
sum(range(100000000))

print("FINISHED", "green")
print(f"Ellapsed time: {time.time() - start} s")

# (class) Robertos-iMac:interpreted_compiled rlucerga$ python py_code_map.py
# FINISHED green
# Ellapsed time: 2.2441558837890625 s

# (base-pypy) Robertos-iMac:interpreted_compiled rlucerga$ python py_code_map.py
# FINISHED green
# Ellapsed time: 0.12270212173461914 s
