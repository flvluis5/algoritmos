# Ejercicio 1
# Jenny has written a function that returns a greeting for a user. However, she's in love with Johnny, and would like to greet him slightly different. She added a special case to her function, but she made a mistake.
# He escrito un codigo que devuelve un saludo al usuario, sin embargo quiero darle un saludo especial
# a mi mejor amigo Juan Jose. He creado esta funcion pero he cometido un fallo. Podemos arreglarlo?

def greet(name):
    return f"Hello, {name}!"
    if name == "Johnny":
        return "Hello, my love!"

# Ejercicio 2
# Crear una funcion que reciba un parametro name como entrada de la función y me devuelva
# name + " toca la guitarra!" o name + " no toca la guitarra..." siempre que el nombre empiece por "R" o "r".

def are_you_playing_guitar(name: str) -> str:
    return name

# Ejercicio 3
# Crear una función que cuente las ovejas que hay en un array. True significa que está la oveja y False significa que
# no esta presente.
# Por ejemplo [True, True, False] -> 3


def count_sheep(sheep):
    return sheep


# Ejercicio 4
# Given a random non-negative number, you have to return the digits of this number within an array in reverse order.
# Dado un numero POSITIVO > 0, crear una funcion que devuelva los digitos de sus numeros en order inverso.
# 35231 => [1,3,2,5,3]
# 0 => [0]
# 984764738 => [8,3,7,4,6,7,4,8,9])


def digitize(n):
    return [n]


# Ejercicio 5
# Crear un programa que encuentre la 'aguja' en una lista de objetos.
# ["Hay", "una", 3, [48,34], "mas cosas", "aguja", "en", "un", "pajar"] --> "Encontrada la aguja en la posición: 5"


def find_needle(lista):
    return lista


# Ejercicio 6
# Write a function that returns the total surface area and volume of a box as an array: [area, volume]
# Crea una function que deuelva una tupla de (area, volumen) de una caja dados w,h,l:
# l es el largo de la caja, o el lado más largo.
# h es la altura de la caja.
# w es el ancho de la caja.
# get_size(4, 2, 6), (88, 48)
# get_size(1, 1, 1), (6, 1)
# get_size(1, 2, 1), (10, 2)
# get_size(1, 2, 2), (16, 4)
# get_size(10, 10, 10), (600, 1000)

def get_size(w,h,l):
    return


# Ejercicio 7
# Piedra papel o tijera con dos jugadores.
# Tiene que devolver "Draw!" o "Player 1 won!" o "Player 2 won!"
# "tijeras", "papel" --> "Player 1 won!"
# "tijeras", "piedra" --> "Player 2 won!"
# "papel", "papel" --> "Draw!"

def ppt(p1, p2):
    return

