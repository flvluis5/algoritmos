import collections
import random
import operator
# exercise 1
import typing
from collections.abc import Sequence
from typing import List, Any, Tuple

"""
Writes the necessary code to return the first and the last item from any known sequence.
The expected output has to be the same type of the sequence!
If I have the string "abc" it has to return "ac".
If the input is a list [1, 2, 3, 4] it has to return [1, 4]
Same for tuple ("hello", "whatever", "world") it has to return ("hello", "world")
Note: Remember you can retrieve any index from a sequence with [] notation.
"""


def first_last_sequence(sequence: Sequence) -> Sequence:
    return sequence[:1] + sequence[-1:]


print(first_last_sequence("abcd"))
print(first_last_sequence((1, 2, 3, 4, 5, 6)))
print(first_last_sequence(["principio", "medio", "final"]))

# exercise 2
"""
Write a code block that loops over a sequence only for the last N elements!
Example: I have a iterable "This is a new string" and 
I only want to iterate over the last 5 positions the output should be: "tring"
"""


def return_n_elements(sequence: Sequence, n: int) -> Sequence:
    """
    Devuelve los N ultimos elementos de una lista
    :param sequence: esta secuencia poyesde ser una lista una tupla o un string
    :param n:
    :return:
    """
    return sequence[-n:]


print(return_n_elements("This is a new string", 5))
print(return_n_elements((1, 2, 3, 4, 5, 6, 7, 8, 9), 5))
print(return_n_elements([1, 2, 3], 5))

numbers = [10, 20, 30, 40, 50, 60]

# exercise 3
"""
Write a piece of code that given a list or a tuple of numbers return a tuple of two elements
The first element in the tuple is the sum of odd numbers
The second element in the tuple is the sum of even numbers
[10, 20, 30, 40, 50, 60] --> (90, 120)
"""


def sum_odds_evens(*numbers_list) -> Tuple[int, int]:
    total_evens = sum([int(number) for number in numbers_list[::2]])
    total_odds = sum([int(number) for number in numbers_list[1::2]])
    return total_evens, total_odds


print(sum_odds_evens(10, 20, 30, 40, 50, 60))

# exercise 4
"""
Code a function game. Guessing the number. We are going to write a game(function) to guess the number between 0 and 100
We will write a function that accepts no arguments and randomly generates a number between 0 and 100 
    (random.randint(0, 100))
We will then let the user to input a number and we will respond back three different choices:
 - Too high
 - Too Low
 - Just right! 
"""


def guessing_game() -> None:
    """
    Guessing a random number with the user input
    """
    answer = random.randint(0, 100)

    while True:
        user_guess = int(input('What is your guess? '))

        if user_guess == answer:
            print(f'Right!  The answer is {user_guess}')
            break

        if user_guess < answer:
            print(f'Your guess of {user_guess} is too low!')
        else:
            print(f'Your guess of {user_guess} is too high!')


print(guessing_game())

# exercise 5
"""
Create a function to join numbers from an iterable of numbers. Use list comprehension
[1,2,3,4,5,6,7,8,9,19] --> "1,2,3,4,5,6,7,8,9,10"
"""


def join_numbers(list_numbers: List[int]) -> typing.AnyStr:
    return ','.join(str(number) for number in list_numbers)


join_numbers([1, 2, 3, 4, 6])

# exercise 6
"""
Given a string sum all the elements that are numbers.
"Hello 10 world20 90" -> 100. world20 is not a valid digit. 
We can use list comprehension here too.
"""


def sum_numbers_in_string(text: str) -> int:
    return sum([int(number) for number in text.split() if number.isdigit()])


print(sum_numbers_in_string(text="10 plus 10 is 20"))
print(sum_numbers_in_string("10 plus 10 is 20"))
assert sum_numbers_in_string(text="10 plus 10 is 20") == 40
print("breakpoint")
# exercise 7
"""
Write a function that adds the string 'ub' before every vowel.
"wind" -> wubind ---> w{ub}ind 
"This is a text" --> "Thubis ubis uba tubext"
"""


def ubbi_dubbi(text: str) -> str:
    """
    Ask the user to enter a sentence/text/word,
    and return the word's translation into Ubbi Dubbi.
    :param text: Input sentence
    :return: Ubbi dubbi text.
    """
    output = []
    for word in text.split():
        for letter in word:
            if letter in 'aeiou':
                output.append(f'ub{letter}')
            else:
                output.append(letter)

    return ''.join(output)


# Exercise 8
"""
We need to find the longest word of an input text
"This is an example" -> example
"""


def longest_word(text: str) -> str:
    """
    find the longest word in a text

    :param text: input text to analyse
    :return: the longest word found in that text
    """
    output = ''
    for one_word in text.split():
        if len(one_word) > len(output):
            output = one_word
    return output


# Exercise 9
"""
We want to hide sensible data from a given string. The sensible data is going to be a list of names that we should hide
and change them for '*'. We need to set as much '*' as the length of the value.

text="this is a text" hide=["text"] --> "this is a ****" 
"""


def remove_sensible_data(text: str, hide: list) -> str:
    """
    Given a string (text) and a list of strings (names),
    replace any occurrence of a name from text with '_' for text we have to hide and return it.
    :param text: text to analyse
    :param hide: Data to hide for the user
    :return:
    """
    output = text

    for one_name in hide:
        output = output.replace(one_name, '_' * len(one_name))

    return output


# Exercise 10
"""
Given a list of dictionaries describing objects from a factory
We need to order them. each dict will have ALWAYS the attributes: name/weight/group
Return the same list of dictionaries ordered by the name of the tool and then from the weight
If passed an empty list, then return an empty list
"""

TOOLS = [{'name': 'hammer', 'weight': 0.5,
          'group': 'home'},
         {'name': 'elevator', 'weight': 250.78,
          'group': 'workshop'},
         {'name': 'pliers', 'weight': 0.3,
          'group': 'home'}
         ]


def alphabetize_names(list_of_dicts):
    return sorted(list_of_dicts, key=operator.itemgetter('name', 'weight'))


print(alphabetize_names(TOOLS))

# exercise 11
"""
Given an array of integers, find the numbers that appears an odd number of times.
There will always be only one integer that appears an odd number of times.

Examples
[7] should return 7, because it occurs 1 time (which is odd).
[0] should return 0, because it occurs 1 time (which is odd).
[1,2] should return [1,2], because it occurs 1 time (which is odd).
[0,1,0,1,0] should return 0, because it occurs 3 times (which is odd).
[1,2,2,3,3,3,4,3,3,3,2,2,1] should return 4, because it appears 1 time (which is odd).

"""
def find_it(seq):
    item = {}
    for x in seq:
        if x in item:
            item[x] += 1
        else:
            item[x] = 1
    for x in item:
        if (item[x] % 2) != 0:
            return x


def find_it2(seq):
    c = collections.Counter(seq)
    return next(iter({number for number, counter in c.items() if counter % 2 == 1}))

# Exercise 12
"""
In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.
"""

def high_and_low(numbers):
    max = None
    min = None
    for number in numbers.split(" "):
        number = int(number)
        if not max or number >= max:
            max = number
        if not min or number <= min:
            min = number
    return f"{max} {min}"


def high_and_low2(numbers):
    nn = [int(s) for s in numbers.split(" ")]
    return f"{max(nn)}, {min(nn)}"
